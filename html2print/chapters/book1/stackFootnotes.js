(function () {
  'use strict';

  function getRegion(el, flow) {
    var regions = flow.getRegionsByContent(el);
    
    if (regions.length > 0) {
      return regions[0];
    }

    return null;
  }

  function getMain(node) {
    if (node.classList.contains('main')) {
      return node;
    }

    return getMain(node.parentNode);
  }

  window.stackFootnotes = function () {
    var ftnts = document.querySelectorAll('.inlineFootnote'),
        flow = document.webkitGetNamedFlows().namedItem('main');

    for (var i=0; i < ftnts.length;i++) {
      var main = getMain(getRegion(ftnts[i], flow));

      if (main.getBoundingClientRect().right > ftnts[i].getBoundingClientRect().right) {
        ftnts[i].classList.add('stacked');
      }
    }
  }
})();