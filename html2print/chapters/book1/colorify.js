(function (window, document) {
    'use strict';

    function makeCanvas (width, height) {
        var canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;
        return canvas;
    }
 
    /*
    * Load from image element
    */
    function fromImg (img) {
        var canvas  = makeCanvas(img.width, img.height),
            ctx     = canvas.getContext('2d');
                                        
        ctx.drawImage(img, 0, 0, img.width, img.height);
        return ctx.getImageData(0, 0, img.width, img.height);
    } 
    
    function inject (img, el) {
        var canvas  = makeCanvas(img.width, img.height);
        canvas.getContext("2d").putImageData(img, 0, 0);
        el.src = canvas.toDataURL();
    }
   
    function colorifyImage (el, color) {
        function exec () {
            el.removeEventListener('load', exec);
            var im = fromImg(el),
            offset = 0;
            
            for (var y=0; y < im.height; y++) {
                for (var x=0; x < im.width; x++) {
                    var newVal      = (0.3 * im.data[offset] + 0.59 * im.data[offset] + 0.11 * im.data[offset]);
                    
                    im.data[offset]      = color[0];
                    im.data[offset+1]    = color[1];
                    im.data[offset+2]    = color[2];
                    im.data[offset+3]    = 255 - newVal;
                    
                    offset          += 4;
                }
            }
            el.addEventListener('load', function () {
                var e = new Event("colorified");
                el.dispatchEvent(e);
            });
            inject(im, el);
        }

        if (el.complete) {
            exec();
        } else {
            el.addEventListener('load', exec, false);
        }
    }

    window.colorify = function (query, color, callback) {
        var els = document.querySelectorAll(query),
            toColorify = els.length,
            colorified = 0;
        if (els.length > 0) {
            for (var i=0; i< els.length; i++)  {
                els[i].addEventListener("colorified", function () {
                    colorified++;
                    if (colorified >= toColorify) {
                        callback();
                    }
                });
                colorifyImage(els[i], color);
            }
        } else {
            callback();
        }
    }

})(window, document);
