;(function (window, document) {
  'use strict';

  function collapseAppendix (node) {
    node.classList.add('collapsed');
    node.classList.remove('expand');
  }

  function expandAppendix (node) {
    node.classList.remove('collapsed');
    node.classList.add('expand');
  }

  function toggleAppendix (node) {
    node.classList.toggle('collapsed');
  }

  function blessAppendixes () {
    var nodes = document.getElementsByTagName('article');
    for (var i=0; i < nodes.length; i++) {
      nodes[i].addEventListener('click', function (e) {
        toggleAppendix(this);
      }, false);
    }
  }

  function collapseAllAppendixes () {
    var nodes = document.getElementsByTagName('article');
    for (var i=0; i < nodes.length; i++) {
      collapseAppendix(nodes[i]);
    }
  }

  function expandAllAppendixes () {
    var nodes = document.getElementsByTagName('article');
    for (var i=0; i < nodes.length; i++) {
      expandAppendix(nodes[i]);
    }
  }

  window.collapseAppendix = collapseAppendix;
  window.expandAppendix = expandAppendix;
  window.toggleAppendix = toggleAppendix;
  window.blessAppendixes = blessAppendixes;
  window.collapseAllAppendixes = collapseAllAppendixes;
  window.expandAllAppendixes = expandAllAppendixes;
})(window, document);
