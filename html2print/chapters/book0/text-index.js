;(function () {
  'use strict';

  window.buildIndex = function (callback) {
    var dummyIndexJSON = "{\"problematisation\":[1,4,6,2,3,10,5,7],\"integrality\":[1,3,5,6,7,10],\"enunciation\":[1,2,10,3,4,8,9,6],\"formulation\":[1,3,4,6,10,2,7,9],\"discursive object\":[1,2,5],\"détournements\":[1],\"lekton\":[2,4,5],\"singularity\":[4,9,2,11,5],\"statement\":[5,10,3,7,9,13],\"history\":[9,1,2,4,5,3,7,11,13,6,14,12],\"urbanism\":[9,1,2,3,5,6,8,10,11,12,4,13,16],\"event\":[10,1,2,3,4,5,8],\"fault line\":[10,4,5],\"birth\":[2,4,5,6,8,11,12,1,3],\"street\":[3,4,12,5,8,6,9,10,11,13,1,2,7,16],\"police\":[3,5,2,10,12],\"palimpsest in action\":[4],\"disappearance\":[4,11,12,9],\"accidental intertwining\":[1],\"ideal city\":[1,2,4,9],\"evil\":[2,3,12,16,9,11,8],\"chance\":[2,4,11,9,10],\"archaeological method\":[3],\"conspiracy\":[3,1,2,4,12,5,6],\"aesthetics\":[3,1,7,9,6,8,10,11],\"ethics\":[4,1,2,7,8,12,3,5,6,9,10],\"provenance\":[5,6,11],\"immanent\":[10,7],\"public domain\":[11,13],\"public space\":[11,12],\"polis\":[12,7,1,3,4,9],\"machine\":[1,2,11,13,4,9],\"concatenation\":[1],\"savoir\":[2,3,5,11,4,9,10,8,12,13,6,1,7],\"medical topography\":[4],\"exercise\":[5,3,11,12,2],\"ventilation\":[6,8,9,11,12],\"problematisations\":[7,10],\"rupture\":[7,4,5,8],\"concatenations\":[1],\"historical subject\":[2],\"vector\":[7,13,3],\"aesthetic\":[8,2,10,12,11],\"focal point\":[10,11,12,1,2,5],\"political territory\":[12,13],\"\":[13],\" as institution\":[13],\"street as institution\":[13],\"arsenal\":[2],\"mathēsis\":[1,5,9,16,4,7],\"wisconst\":[1,5,6,9,16,4,7,8],\"resemblance\":[2,5,13,14,15,7],\"‘as if’\":[2,5],\"ars combinatoria\":[5,6],\"civil life\":[5,12,16,1,9,3,4,6,7,8],\"expert\":[5,6,7,9],\"constancy\":[5,6,4,7,8,9,10,11,12,1,2,3],\"dissimulation\":[8,9],\"vital ascesis\":[12,1],\"subject of initiative\":[3],\"dispositif\":[3,11],\"myth\":[4],\"gemüt\":[5,8],\"technologies of the self\":[5,6,8,2],\"structural dissidence\":[11],\"inertia\":[12],\"indifference\":[10],\"vita politica\":[1,4],\"passivum\":[10],\"ghemoed\":[8],\"hypermodern dilettante\":[9]}",
        indexJSON = window.localStorage.getItem('the-riddle-index'),
        index = (indexJSON) ? JSON.parse(indexJSON) : JSON.parse(dummyIndexJSON),
        indexContainer = document.querySelector('#index'),
        keys = Object.keys(index);

    keys.sort();

    for (var k = 0; k < keys.length; k++) {
      var key = keys[k],
          pages = index[key];

      pages.sort(function (a, b) {
        if (a < b) {
          return -1;
        } else {
          if (a > b) {
            return 1;
          }

          return 0;
        }
      });

      console.log(pages);

      var entry = document.createElement('p'),
          entryTextString = key + ': '

      for (var i=0; i < pages.length; i++) {
        entryTextString += pages[i] + ', ';
      }

      entry.appendChild(document.createTextNode(entryTextString));
      indexContainer.appendChild(entry);
    }

    callback();
  };
})();