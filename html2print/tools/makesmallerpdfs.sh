#! /bin/bash

bookfolder=${1%/}

rm ${bookfolder}/pdfs/*-downsampled.pdf

for pdffile in $bookfolder/pdfs/*.pdf; do
  gs \
  -o "${pdffile%.pdf}-downsampled.pdf" \
  -sDEVICE=pdfwrite \
  -dDownsampleColorImages=true \
  -dDownsampleGrayImages=true \
  -dDownsampleMonoImages=true \
  -dColorImageResolution=150 \
  -dGrayImageResolution=150 \
  -dMonoImageResolution=150 \
  -dColorImageDownsampleThreshold=1.0 \
  -dGrayImageDownsampleThreshold=1.0 \
  -dMonoImageDownsampleThreshold=1.0 \
   "${pdffile}"
done