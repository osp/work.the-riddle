#! /bin/bash

path=$1 
folder=$(dirname "${path}")
htmlfile=$(basename "${path}")
pdffile="${htmlfile%.html}.pdf"

OSPKitPDF "http://html2print/${path}" "${folder}/pdfs/${pdffile}"