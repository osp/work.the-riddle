#! /bin/bash

bookfolder=${1%/}
total=0

for pdffile in $bookfolder/pdfs/*.pdf; do
  pages=$(pdfinfo "${pdffile}" | awk '/Pages/ {print $2}')
  echo "${pdffile} - ${pages}"
  let total+=pages
done

echo "Total in book - ${total}"
