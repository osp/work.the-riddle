#! /bin/bash

bookfolder=${1%/}

for htmlfile in $bookfolder/*.html; do
  basefilename=$(basename "${htmlfile%.html}")
  if [ "${basefilename}" != "template" ]; then
    #echo "${chapterfile%.*}".pdf
  OSPKitPDF "http://html2print/${htmlfile}" "${bookfolder}/pdfs/${basefilename}.pdf"
  fi
done