replace_patt = '++$1++'

for k in keywords:
  patt = '[^|\s]{0}[\s|$]'.format(k)
  md = re.subn(patt, replace_patt, md)

\+\+(.[^\+]+)\+\+
<span class="keyword"></span>

#! /bin/bash
sourcefolder=${1%/}

for book in $sourcefolder/book*; do
    echo "Reading ${book}"
    
    for htmlfile in $book/*.html; do
        echo "<article data-src=\"${htmlfile}\"></article>"
    done;
done;

echo "Applying extra line breaks"