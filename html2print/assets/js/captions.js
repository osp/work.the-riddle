;(function () {
  'use strict';
  
  window.setTimeout(function () {
    'use strict';

    var caption, img, imgs = document.getElementsByTagName('img');

    for (var i=0; i < imgs.length; i++) {
      img = imgs[i];
      caption = document.createElement('figcaption');
      caption.appendChild(document.createTextNode(img.getAttribute('alt')));
      img.parentNode.insertBefore(caption, img.nextSibling);
    }
  }, 5000);
})();