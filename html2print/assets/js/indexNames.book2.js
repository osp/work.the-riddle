;(function () {
  'use strict';


  if ((!document.getNamedFlows) && document.webkitGetNamedFlows) {
    document.getNamedFlows = document.webkitGetNamedFlows;
  }
  
  if (!document.getNamedFlow) {
    document.getNamedFlow = function (name) {
      return document.getNamedFlows(name).namedItem(name);
    }
  }

  var textFlow = document.getNamedFlow('main'),
      captionsFlow = document.getNamedFlow('figcaptions');

  /**
   * Returns first region the given node is shown within
   * 
   * workaround for getRegionsForContent() as that function only seems to work 
   * on block-elements
   */
  function getRegionByNode(flow, node) {
    var ranges, regions = flow.getRegionsByContent(node);

    if (regions.length > 0) {
      return regions[0];
    } else {
      regions = flow.getRegions();

      for (var i=0; i < regions.length; i++) {
        ranges = regions[i].webkitGetRegionFlowRanges();

        for (var r=0; r < ranges.length; r++) {
          if (ranges[r].isPointInRange(node, 0)) {
            return regions[i];
          }
        }
      }
      console.log('Could not find region for node', node);
    }
  }

  /**
   * Find page for given node, travels up the DOM tree to find
   * first wrapping paper element. Does not work for Nodes in a flow.
   */
  function getPageByNode (node) {
    if (node) {
      if (node.classList.contains('paper')) { 
        return node;
      } else {
        if (node.parentElement) {
          return getPageByNode(node.parentElement);
        } else {
          return false;
        }
      }
      return false;
    }
  }

  /**
   * Get page by node in region
   */
  function getPageByRegionNode (flow, node) {
    var region = getRegionByNode(flow, node);
    if (region) {
      return getPageByNode(getRegionByNode(flow, node));
    } else {
      return null;
    }
  }

  /**
   * Return page number for given page
   */
  function getPageNum (page) {
    var firstpagenum = parseInt(document.querySelector('.paper:first-of-type').id.replace(/\D/g, '')),
        pagenum = parseInt(page.id.replace(/\D/g, ''));

    return (pagenum - firstpagenum) * 2 + firstpagenum + 1;
  }

  window.indexNames = function () {
    var keywords = document.querySelectorAll('span.indexed_name'),
        storedIndex = window.localStorage.getItem('the-riddle-name-index'),
        index = (storedIndex != null) ? JSON.parse(storedIndex) : {};
        

    for (var i=0; i<keywords.length;i++) {
      var region = getRegionByNode(textFlow, keywords[i]);
      
      if (!region) {
        region = getRegionByNode(captionsFlow, keywords[i]);
      }

      if (region) {
        var keywordPage = getPageByNode(region);

        if (keywordPage) {
          var keyword = keywords[i].innerText.toLowerCase(),
              pageNum = getPageNum(keywordPage);

          if (region.classList.contains('secondary')) {
            pageNum += 1;
          }

          if (!(keyword in index)) {
            index[keyword] = new Array();
          }

          if (!(index[keyword].includes(pageNum))) {
            index[keyword].push(pageNum);
          }       
        }
      }
    }

    console.log(index);
    window.localStorage.setItem('the-riddle-name-index', JSON.stringify(index));
  };
})();