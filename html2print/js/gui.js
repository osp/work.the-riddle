(function($, window, undefined) {
    'use strict';

    var $viewport = $("#viewport")[0]
        , $previewBtn = $('[name="preview"]')[0]
        , $gridBtn = $('[name="grid"]')[0]
        , $debugBtn = $('[name="debug"]')[0]
        , $spreadBtn = $('[name="spread"]')[0]
        , $zoomBtn = $('[name="zoom"]')[0]
        , $pageBtn = $('[name="page"]')[0]
        , $displayBtn = $('[name="display"]')[0]
        , $reloadBtn = $('#reload')[0]
        , $printBtn = $('#print')[0]
        , $designmodeBtn = $('[name="designmode"]')[0]
        , $expandButton = $('#expand')[0]
        , $checkBtn = $('#check')[0];
    ;

    window.addEventListener("load", function(event) {
        function switchPreview(event) {
            var $doc = $viewport.contentDocument.getElementsByTagName('html')[0]; 
            if(this.checked) {
                $doc.classList.add("preview");
                $doc.classList.remove("normal");
            } else {
                $doc.classList.add("normal");
                $doc.classList.remove("preview");
            }
        }

        function switchGrid(event) {
           var $doc = $viewport.contentDocument.getElementsByTagName('html')[0];
            if (this.checked) {
                $doc.classList.add("grid");
            } else {
                $doc.classList.remove("grid");
            }
        }

        function switchDebug(event) {
            var $doc = $viewport.contentDocument.getElementsByTagName('html')[0];
            if(this.checked) {
                $doc.classList.add("debug");
            } else {
                $doc.classList.remove("debug");
            }
        }

        function switchSpread(event) {
            var $doc = $viewport.contentDocument.getElementsByTagName('html')[0];
            if(this.checked) {
                $doc.classList.add("spread");
            } else {
                $doc.classList.remove("spread");
            }
        }

        function switchDesignMode(event) {
            if(this.checked) {
                $viewport.contentDocument.designMode = "on";
            } else {
                $viewport.contentDocument.designMode = "off";
            }
        }

        function setZoom(event) {

            var zoomLevel = this.value / 100;
            var elt = $viewport.contentDocument.querySelector("#pages");

            if (elt && elt.style) {
                elt.style.webkitTransform = "scale(" + zoomLevel + ")";
                elt.style.webkitTransformOrigin = "0 0";
            }
        }

        function changePage(event) {
            var $doc = $viewport.contentDocument.getElementsByTagName('html')[0];

            var pageNumber = this.value - 1;

            var target = $doc.querySelectorAll('.paper')[pageNumber];
            var offsetTop = target.offsetTop;

            $doc.querySelector('body').scrollTop = offsetTop;
        }

        function changeDisplay(event) {
            var $doc = $viewport.contentDocument.getElementsByTagName('html')[0];
            
            var htmlelt = $doc.querySelectorAll('html')[0];
            var elts = $doc.querySelectorAll('img');

            $doc.classList.remove("low");
            $doc.classList.remove("bw");
            $doc.classList.remove("color");
            $doc.classList.add(this.value);

            for (var i = 0, l = elts.length; i < l; i ++) {
                var elt = elts[i];

                if (!elt.dataset.low) { elt.dataset.low = elt.src; }

                elt.style.visibility = 'visible';

                if (elt.dataset[this.value]) {
                    elt.src = elt.dataset[this.value];
                } else {
                    elt.src = "";
                    elt.style.visibility = 'hidden'
                }
            }
        }

        function reload(event) {
           $viewport.contentWindow.location.reload();
        }

        function print(event) {
            $viewport.contentWindow.print();
        }

        function expand() {
            var event = new Event("expand");
            $viewport.contentDocument.dispatchEvent(event);
        }

        function checkRes() {
           (function () {
                var img, color, images = $viewport.contentDocument.querySelectorAll('img');

                for (var i=0; i < images.length; i++) {
                    img = images[i];
                    img.style.outline = '';
                    if (
                    img.naturalHeight < (img.height * (300 / 96)) || 
                    img.naturalWidth < (img.width * (300 / 96))
                    ) {
                    color = 'yellow';

                    if (
                        img.naturalHeight < (img.height * (200 / 96)) || 
                        img.naturalWidth < (img.width * (200 / 96))
                    ) {
                        color = 'orange';

                        if (
                        img.naturalHeight < (img.height * (150 / 96)) || 
                        img.naturalWidth < (img.width * (150 / 96))
                        ) {
                        color = 'red';
                        }
                    }

                    img.parentNode.style.outline = '5px solid ' + color;
                    }
                }

            })();
        }

//         $previewBtn.removeEventListener("change", switchPreview);
//         $gridBtn.removeEventListener("change");
//         $debugBtn.removeEventListener("change");
//         $spreadBtn.removeEventListener("change");
//         $zoomBtn.removeEventListener("change");
//         $pageBtn.removeEventListener("change");
//         $reloadBtn.removeEventListener("click");
//         $printBtn.removeEventListener("click");
//         $displayBtn.removeEventListener("change");
//         $designmodeBtn.removeEventListener("change");
        
//         $previewBtn.addEventListener("change", switchPreview);
//         $gridBtn.addEventListener("change", switchGrid);
//         $debugBtn.addEventListener("change", switchDebug);
        $spreadBtn.addEventListener("change", switchSpread);
        $zoomBtn.addEventListener("change", setZoom);
        $pageBtn.addEventListener("change", changePage);
        $checkBtn.addEventListener("click", checkRes);
        $reloadBtn.addEventListener("click", reload);
        $printBtn.addEventListener("click", print);
        $expandButton.addEventListener("click", expand); //         $displayBtn.addEventListener("change", changeDisplay);
//         $designmodeBtn.addEventListener("change", switchDesignMode);

//         switchPreview.bind($previewBtn)();
//         switchGrid.bind($gridBtn)();
//         switchDebug.bind($debugBtn)();
//         switchSpread.bind($spreadBtn)();
        setZoom.bind($zoomBtn)();
        //changePage.bind($pageBtn)();
    }, false);
})(document.querySelectorAll.bind(document), window);
