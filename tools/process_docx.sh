#! /bin/bash

infolder=${1%/}
outfolder=${2%/}

if [ ! -d "${outfolder}" ]; then
	mkdir "${outfolder}" "${outfolder}/img"
else
	if [ ! -d "${outfolder}/img" ]; then
		mkdir "${outfolder}/img"
	fi
fi

for infile in ${infolder}/*.docx; do
    echo "Converting ${infile}"
    outbase=$(basename "${infile}" .docx)
    outfile="${outfolder}/${outbase}.md"

    if [ ! -f "${outfile}" ]; then
	    pandoc "${infile}" -r docx -t markdown -s -o "${outfile}"
    else
        echo "${mdfile} already exists"
    fi
done;

# Copy over images
for ext in 'jpg' 'JPG' 'jpeg' 'JPEG' 'gif' 'GIF' 'tiff' 'TIFF' 'tif' 'TIF' 'png' 'PNG'; do
	for infile in $infolder/*.$ext; do
		outbase=$(basename "${infile}")
		outfile="${outfolder}/img/${outbase}"

		if [ -f "${infile}" ] && [ ! -f "${outfile}" ]; then
		 	echo "Copying ${infile}"
			cp "${infile}" "${outfile}"
		fi
	done;
done;
